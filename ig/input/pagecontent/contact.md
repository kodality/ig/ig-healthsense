If you have any issue about Implementation Guide please create the Github ticket on the [project site](https://gitlab.com/kodality/ig/ig-healthsense/-/issues).

If you need further information or wish to provide feedback on this implementation guide, please e-mail the appropriate organisation below:
- [Igor Bossenko](mailto:igor.bossenko@taltech.ee)
- [Mall Maasik](mailto:mall.maasik@taltech.ee)


