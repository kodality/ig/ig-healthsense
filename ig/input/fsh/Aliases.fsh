Alias: SNOMED = http://snomed.info/sct
Alias: UCUM = http://unitsofmeasure.org
Alias: LOINC = http://loinc.org
//Alias: $ATC = urn:oid:2.16.840.1.113883.6.73
//Alias: $SPECIALITY = urn:oid:1.3.6.1.4.1.28284.6.2.1.4.5
//Alias: $PractROLE = urn:oid:1.3.6.1.4.1.28284.6.2.2.14.4

/* Eesti Riigi Infosüsteemide keskus */
Alias: RIK = https://ariregister.rik.ee

//Alias: TIME-UNITS = http://hl7.org/fhir/ValueSet/all-time-units
