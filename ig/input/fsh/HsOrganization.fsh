
Instance: ITK
InstanceOf: Organization
Title:          "Organizations - Hospital ITK"
Description:    "Ida-Tallinna Keskhaigla"
Usage: #example
* id = "org1"
* name = "East Tallinn Central Hospital"
* identifier.system = RIK
* identifier.value = "10822068"
* alias = "ITK"
/*
* contact
  * telecom.system = #email
  * telecom.value = "info@itk.com"
  * address.line = "Ravi tn 18"
  * address.city = "Tallinn"
*/

Instance: Synlab
InstanceOf: Organization
Title:          "Organizations - Laboratory Synlab"
Description:    "Synlab labor"
Usage: #example
* id = "synlab"
* name = "SYNLAB EESTI OÜ"
* identifier.system = RIK
* identifier.value = "11107913"
* alias = "Synlab"
/*
* contact
  * telecom.system = #email
  * telecom.value = "info@synlab.com"
  * address.line = "Veerenni tn 53a"
  * address.city = "Tallinn"
*/

Instance: Tervisekassa
InstanceOf: Organization
Title:          "Organizations - Estonian HIF"
Description:    "Eesti Tervisekassa"
Usage: #example
* id = "ins"
* name = "Eesti Tervisekassa"
* identifier.system = RIK
* identifier.value = "74000091"
* alias = "EHK"
/*
* contact
  * telecom.system = #email
  * telecom.value = "info@tervisekassa.ee"
  * address.line = "Lastekodu 88"
  * address.city = "Tallinn"
*/
